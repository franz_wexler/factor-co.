#########################################
try:
    import pygame
    from pygame import *
    from decimal import Decimal
    from random import randint, choice
    import os.path, inspect
    
    pygame.init()
    global screen
    screen=pygame.display.set_mode()
    screen=display.set_mode(screen.get_size(), RESIZABLE|DOUBLEBUF) #(1024, 512+192)
    display.set_caption("Factor&Co.", "F&Co.")
    display.set_icon(image.load("./icons/mainappicon.gif"))
    clock=time.Clock()
    headertext=font.Font(None, 48)
    normaltext=font.Font(font.match_font('Courier'), 19)
    
    terrains=eval(open("./config/terrains.txt").read())
    for element in terrains:
        if terrains[element]["basic"]!=None:
            terrains[element]["basic"]=image.load("./terrains/"+terrains[element]["basic"]).convert()
        else:
            terrains[element]["basic"]=Surface((0, 0))
        terrains[element]["ground"]=image.load("./terrains/"+terrains[element]["ground"])
        terrains[element]["count"]=0
    del element
    
    
    mixer.music.load("./soundtrack/clearday.mp3")
    
    #mixer.music.play()
    #mixer.music.set_endevent(USEREVENT+2)
    #musics=eval(open("./config/music.txt").read())
    
    
    selection=Surface((32, 32))
    selection.fill((200,200,200))
    
    demolishselection=Surface((32, 32))
    demolishselection.fill((200,200,200))
    draw.rect(demolishselection, (250, 50, 50), Rect(0, 0, 32, 32), 1)
    
    badselection=Surface((32, 32))
    badselection.fill((250,50,50))
    
    class World():
        def __init__(self, name="UNNAMED", size=[16, 16], generate=True, update=True, random=False):
            self.shift=(0, -2)
            self.size=size
            self.name=name
            if generate:
                if random:
                    self.generate(random_file='random')
                else:
                    self.generate()
            if update:
                self.update()
            
        def save(self, filepath="./saves/{0}.f-co"):
            s_data=[[[self.tilemap[x][y].terrainname for y in range(self.size[1])]
                 for x in range(self.size[0])]] #[0]
            s_data.append({}) #[1]
            for element in terrains:
                s_data[1][element]=terrains[element]["count"]
            s_data.append(None) #[2]
            s_data.append(Stats.harvested_plants) #[3]
            s_data.append(Stats.game_resources) #[4]
            s_data.append(Stats.resources_buyprice) #[5]
            s_data.append(Stats.resources_sellprice) #[6]
            s_data.append(Stats.resources_buymulti)  #7
            s_data.append(Stats.resources_sellmulti) #8
            s_data.append(Stats.max_hrvest_plants) #9
            f=open(filepath.format(self.name), 'w')
            f.write(str(s_data))
            f.close()
            return (os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
        def strings_to_tiles(self):
            for x in range(self.size[0]):
                for y in range(self.size[1]):
                    self.tilemap[x][y]=Tile(self.tilemap[x][y])
        def generate(self, path="./saves/{0}.f-co", random_file='file'):
            if os.path.isfile(path.format(self.name)):
                f=open(path.format(self.name))
                temp=list(eval(f.read()))
                f.close()
                self.tilemap=temp[0]
                for el in temp[1]:
                    terrains[el]["count"]=temp[1][el]
                #temp[2]
                if temp[4][9]>=100:
                    Stats.anti_discovered=True
                Stats.harvested_plants=temp[3]
                Stats.game_resources=temp[4]
                Stats.resources_buyprice=temp[5]
                Stats.resources_sellprice=temp[6]
                Stats.resources_buymulti=temp[7]
                Stats.resources_sellmulti=temp[8]
                Stats.max_hrvest_plants=temp[9]
                self.size[0]=len(self.tilemap)
                self.size[1]=len(self.tilemap[0])
                self.strings_to_tiles()
            else:
                if random_file=='file':
                    def generate_tile(x, y):
                        if x%2==0 and y%2==0:
                            return "Heavy Forest"
                        else:
                            return "Concrete"
                    self.tilemap=[[generate_tile(x, y) for x in range(self.size[0])]for y in range(self.size[1])]
                    self.strings_to_tiles()
                    Description.change("Generated world '"+self.name+"' saved in "+str(self.save())+"/saves/"+self.name)
                elif random_file=='random':
                    terrainslist=list(terrains)
                    terrainslist.remove("Pinky House") #Do not show easter eggs !!!
                    terrainslist.remove("City Town Hall")
                    def generate_tile(x, y):
                        return choice(terrainslist)
                    self.tilemap=[[generate_tile(x, y) for x in range(self.size[0])]for y in range(self.size[1])]
                    self.strings_to_tiles()
            
        def update(self):
            self.basicsurface=Surface((32*(self.size[0]-self.shift[0]), 32*(self.size[1]-self.shift[1])))
            self.basicsurface.fill((0,0,255))
            self.basicsurface.set_colorkey((0,0,255))
            self.groundsurface=Surface((32*self.size[0], 32*self.size[1]))
            for x in range(self.size[0]):
                for y in range(self.size[1]):
                    self.groundsurface.blit(self.tilemap[x][y].ground, (x*32, y*32))
                    self.basicsurface.blit(self.tilemap[x][y].basic,
                        ((x-self.shift[0])*32+self.tilemap[x][y].shift[0],
                         (y-self.shift[1])*32+self.tilemap[x][y].shift[1]))
        def resetchanges(self):
            self.generate()
            
    class Tile():
        def __init__(self, terrainname):
            self.config(terrainname)
        def config(self, new_terrainname):
            self.terrainname=new_terrainname
            self.ground=terrains[new_terrainname]["ground"]
            self.basic=terrains[new_terrainname]["basic"]
            self.shift=terrains[new_terrainname]["shift"]
            self.build_price=terrains[new_terrainname]["build price"]
            self.demolition_price=terrains[new_terrainname]["demolition price"]
            self.description=terrains[new_terrainname]["description"]
    
    menu_background=image.load("./othergraphics/menubackground.gif").convert()
    class View():
        dragging=False
        drag_pos=None
        world_pos=[512, 512]
        @staticmethod
        def build():
            var.current_world.update()
            size=screen.get_size()
            size2=var.current_world.groundsurface.get_size()
            View.world_pos=[size[0]/2-size2[0]/2, size[1]/2-size2[1]/2]
        @staticmethod
        def update():
            screen.blit(var.current_world.groundsurface, View.world_pos)
            w = View.world_pos
            aftersurfaceblit=False
            if not True in [r.collidepoint(mouse_pos) for r in GUIrects]:
                if m[0]>=w[0] and m[0]<w[0]+(var.current_world.size[0])*32:
                    if(m[1]>=w[1] and m[1]<w[1]+(var.current_world.size[1])*32 ):
                        if Tool.mode==Tool.BUILD:
                            tilepos=((int((m[0]-w[0])/32)), int((m[1]-w[1])/32))
                            if var.current_world.tilemap[tilepos[0]][tilepos[1]].build_price!=None:
                                screen.blit(badselection, ((int((m[0]-w[0])/32)*32+w[0]), int((m[1]-w[1])/32)*32+w[1]))
                            else:
                                aftersurfaceblit=True
                        elif Tool.mode==Tool.SELECT:
                            screen.blit(selection,
                                    ((int((m[0]-w[0])/32)*32+w[0]), int((m[1]-w[1])/32)*32+w[1]))
                            Description.change(var.current_world.tilemap[int((m[0]-View.world_pos[0])/32)][int((m[1]-View.world_pos[1])/32)].terrainname)
                        elif Tool.mode==Tool.DESTROY:
                            if var.current_world.tilemap[int((m[0]-w[0])/32)][int((m[1]-w[1])/32)].terrainname!="Concrete":
                                screen.blit(demolishselection,
                                    ((int((m[0]-w[0])/32)*32+w[0]), int((m[1]-w[1])/32)*32+w[1]))
                            else:
                                screen.blit(selection,
                                    ((int((m[0]-w[0])/32)*32+w[0]), int((m[1]-w[1])/32)*32+w[1]))
                    else:
                        Description.change("", "")
                else:
                    Description.change("", "")
            screen.blit(var.current_world.basicsurface,
                (View.world_pos[0]+var.current_world.shift[0]*32,
                 View.world_pos[1]+var.current_world.shift[1]*32))
            if aftersurfaceblit:
                screen.blit(terrains[Tool.data[0]]["basic"], ((int((m[0]-w[0])/32)*32+w[0]+terrains[Tool.data[0]]["shift"][0]), int((m[1]-w[1])/32)*32+w[1]+terrains[Tool.data[0]]["shift"][1]))
           
    class BuildMenu():
        surface, rect, world=None, None, None
        selectrect=None
        pos=(0, 0)
        size=(0, 0)
        toogledbutton=False
        @staticmethod
        def build():
            BuildMenu.rect=Rect((BuildMenu.pos[0], BuildMenu.pos[1]+16), (BuildMenu.size[0], BuildMenu.size[1]-16))
            GUIrects.append(BuildMenu.rect)
            BuildMenu.bsurface=Surface(BuildMenu.size)
            BuildMenu.bsurface.blit(menu_background, (0, 0))
            BuildMenu.bsurface.blit(menu_background, (0, 256))
            BuildMenu.world=World("__BUILDMENU__", [1, 10], False, False)
            BuildMenu.world.tilemap=[["Basic Factory", "Standard Factory", "Advanced Factory", "Product Factory",
                                      "Better Product Factory", "Super Product Factory", "Steam Powerplant", "Nuclear Powerplant",
                                      "Antimatter Powerplant", "Factor&Co. Research Facility"]]
            BuildMenu.world.strings_to_tiles()
            BuildMenu.world.update()
            draw.line(BuildMenu.world.basicsurface, (0, 0, 0), (0, 64), (0, 32*10+32), 1)
            draw.line(BuildMenu.world.basicsurface, (0, 0, 0), (31, 64), (31, 32*10+32), 1)
            BuildMenu.selectrect=Rect((BuildMenu.pos[0], BuildMenu.pos[1]+32), (256, 32*10))
            BuildMenu.buttrect=Rect((BuildMenu.pos[0]+BuildMenu.size[0]-24-4, 0), (32, 32*10))
            temp_s=Surface((64, 32*13))
            temp_s.fill((255,0,0))
            temp_s.set_colorkey((255,0,0))
            for i in range(1, 10+1):
                temp_s.blit(buy_b_icon, (0, 4+32*i))
            
            BuildMenu.surface=Surface(BuildMenu.size)
            BuildMenu.surface.fill((200, 200, 200))
            BuildMenu.surface.set_colorkey((200, 200, 200))
            BuildMenu.surface.blit(temp_s, (0, 0))
        @staticmethod
        def update():
            screen.blit(BuildMenu.bsurface, BuildMenu.pos)
            screen.blit(BuildMenu.world.groundsurface, (BuildMenu.pos[0]+2, BuildMenu.pos[1]+32))
            if BuildMenu.selectrect.collidepoint(mouse_pos):
                if (mouse_pos[0]-BuildMenu.buttrect.topleft[0]) in range(4, 27):
                    BuildMenu.isbuttonactive=True
                    screen.blit(selection24, (BuildMenu.pos[0]+BuildMenu.size[0]-28, int(mouse_pos[1]/32)*32+4))
                    Description.change("Buy new "+BuildMenu.world.tilemap[0][int((mouse_pos[1]-32)/32)].terrainname+" for $"+str(BuildMenu.world.tilemap[0][int((mouse_pos[1]-32)/32)].build_price)+".", "Buy new building")
                else:
                    BuildMenu.isbuttonactive=False
                    screen.blit(Stats.selectionstrip, (BuildMenu.pos[0], int(mouse_pos[1]/32)*32))
                    Description.change(BuildMenu.world.tilemap[0][int((mouse_pos[1]-32)/32)].terrainname)
            elif mouse_pos[0] in range(BuildMenu.pos[0]+4, BuildMenu.pos[0]+28) and mouse_pos[1] in range(4+32*13, 28+32*13):
                screen.blit(selection24, (BuildMenu.pos[0]+4, 4+32*13))
                Description.change("Make buildings and trees die with AMAZING iBuldoze.", "Demolition Tool")
                BuildMenu.isbuttonactive=True
            else:
                BuildMenu.isbuttonactive=False
            if  BuildMenu.toogledbutton:
                screen.blit(selection24, (BuildMenu.pos[0]+4, 4+32*13))
            screen.blit(BuildMenu.surface, BuildMenu.buttrect)
            screen.blit(demolish_icon, (BuildMenu.pos[0]+4, 4+32*13))
            screen.blit(BuildMenu.world.basicsurface, (BuildMenu.pos[0]+2, BuildMenu.pos[1]-(64-32)))
        def buttonaction(y):
            if y in range(0, 12):
                BuildMenu.toogledbutton=False
                Tool.data[0]=BuildMenu.world.tilemap[0][y].terrainname
                Tool.data[1]=BuildMenu.world.tilemap[0][y].build_price
                Tool.mode=Tool.BUILD
                Description.change("Select tile to place new "+BuildMenu.world.tilemap[0][y].terrainname+".", "Place new building", forcelevel=2)
            elif y==12:
                BuildMenu.toogledbutton=not BuildMenu.toogledbutton
                if not BuildMenu.toogledbutton:
                    Tool.mode=Tool.SELECT
                    Description.resetforcelevel()
                else:
                    Tool.mode=Tool.DESTROY
                    Description.change("Select tile you want to clean from buildings, plants, birds, town halls, little kittens and place a beautiful, modern Concrete", "Demolish tile", forcelevel=2)
    
    def get_correct_string_num(num):
        #if len(str(num))>7:
        #    return "{:.2E}".format(Decimal(str(num))).replace('+', '')
       # else:
        #    return str(num)
        return str(round(num, 1))
    
    selection24=Surface((24, 24))
    selection24.fill((255, 255, 255))
    
    sell_icon=Surface((24, 24))
    sell_icon.fill((255, 0, 0))
    sell_icon.set_colorkey((255,0,0))
    draw.rect(sell_icon, (0, 0, 0), Rect((0, 0), (24, 24)), 1)
    sell_icon.blit(image.load("./icons/sell.gif").convert(), (4, 4))
    
    buy_icon=Surface((24, 24))
    buy_icon.fill((255, 0, 0))
    buy_icon.set_colorkey((255,0,0))
    draw.rect(buy_icon, (0, 0, 0), Rect((0, 0), (24, 24)), 1)
    buy_icon.blit(image.load("./icons/buymore.gif").convert(), (4, 4))
    
    buy_b_icon=Surface((24, 24))
    buy_b_icon.fill((255, 0, 0))
    buy_b_icon.set_colorkey((255,0,0))
    draw.rect(buy_b_icon, (0, 0, 0), Rect((0, 0), (24, 24)), 1)
    buy_b_icon.blit(image.load("./icons/placenewbuilding.gif").convert(), (4, 4))
    
    demolish_icon=Surface((24, 24))
    demolish_icon.fill((255, 0, 0))
    demolish_icon.set_colorkey((255,0,0))
    draw.rect(demolish_icon, (0, 0, 0), Rect((0, 0), (24, 24)), 1)
    demolish_icon.blit(image.load("./icons/destroy.gif").convert(), (4, 4))
    
    class Timeline():
        pos, size=(0, 0), (0, 0)
        usurface, surface, chartsurface, chartsizesurface, psurface=None, None, None, None, None
        rect=None
        resrccolors=[(110, 110, 110), (180, 70, 0), (100, 100, 230), (0, 230, 255)]
        lastvalues=[0,0,0,0]
        def price_average(index):
            return (Stats.resources_buyprice[index]+Stats.resources_sellprice[index])/2
        def resrcval(index, average_buy_sell="average"):
            if average_buy_sell=="average":                                                                                                                     #I want to play agar.io
                if index==S_FUEL:                                                                                                                          #I was really big cell before -
                    return Timeline.price_average(COAL)/2                                                                                                  #My brother taken my internet!
                elif index==S_COMP:                                                                                                                        #So now I can only do my game.
                    return Timeline.price_average(COMP1)/2                                                                                               #Oh, and don't tell him that these comments aren't about this code.
                elif index==S_PRODUCT:
                    return Timeline.price_average(PRODUCT)/14
                elif index==S_ENERGY:
                    return Timeline.price_average(ENERGY)/1
            elif average_buy_sell=="buy":
                if index==S_FUEL:
                    return Stats.resources_buyprice[COAL]/2
                elif index==S_COMP:
                    return Stats.resources_buyprice[COMP1]/2
                elif index==S_PRODUCT:
                    return Stats.resources_buyprice[PRODUCT]/14
                elif index==S_ENERGY:
                    return Stats.resources_buyprice[ENERGY]/1
            elif average_buy_sell=="sell":
                if index==S_FUEL:
                    return Stats.resources_sellprice[COAL]/2
                elif index==S_COMP:
                    return Stats.resources_sellprice[COMP1]/2
                elif index==S_PRODUCT:
                    return Stats.resources_sellprice[PRODUCT]/14
                elif index==S_ENERGY:
                    return Stats.resources_sellprice[ENERGY]/1
        def build():
            Timeline.rect=Rect(Timeline.pos, Timeline.size)
            GUIrects.append(Timeline.rect)
            
            Timeline.pricepointers=[COAL, COMP1, PRODUCT, ENERGY]
            Timeline.usurface=Surface(Timeline.size)
            Timeline.usurface.blit(menu_background, (0, 0))
            Timeline.usurface.blit(menu_background, (512, 0))
            Timeline.usurface.blit(menu_background, (1024, 0))
            Timeline.usurface.blit(menu_background, (1536, 0))
            Timeline.usurface.blit(menu_background, (2048, 0))
            b=8
            draw.rect(Timeline.usurface, (0, 230, 255), ((4, b), (4+22, 4+22)))
            draw.rect(Timeline.usurface, (0, 0, 0), ((4, b), (4+22, 4+22)), 1)
            Timeline.usurface.blit(image.load("./icons/energy.gif").convert(), (5, 1+b))
            draw.rect(Timeline.usurface, (180, 70, 0), ((4, 28+b), (26, 26)))
            draw.rect(Timeline.usurface, (0, 0, 0), ((4, 28+b), (26, 26)), 1)
            Timeline.usurface.blit(image.load("./icons/component3.gif").convert(), (5, 29+b))
            draw.rect(Timeline.usurface, (110, 110, 110), ((4, 56+b), (26, 26)))
            draw.rect(Timeline.usurface, (0, 0, 0), ((4, 56+b), (26, 26)), 1)
            Timeline.usurface.blit(image.load("./icons/coal.gif").convert(), (5, 57+b))
            draw.rect(Timeline.usurface, (100, 100, 230), ((4, 84+b), (26, 26)))
            draw.rect(Timeline.usurface, (0, 0, 0), ((4, 84+b), (26, 26)), 1)
            Timeline.usurface.blit(image.load("./icons/product.gif").convert(), (5, 85+b))
            
            draw.rect(Timeline.usurface, (0, 0, 0), ((127, 4), (Timeline.size[0]-196, Timeline.size[1]-8)), 1)
            Timeline.chartsizesurface=Surface((Timeline.size[0]-198, 118))
            Timeline.chartsizesurface.fill((240, 240, 240))
            if Timeline.chartsurface==None:
                Timeline.chartsurface=Surface((2048, 118))
                Timeline.chartsurface.fill((240, 240, 240))
            Timeline.psurface=Surface((90, 118))
            Timeline.gametick_update()
        def update():
            screen.blit(Timeline.usurface, Timeline.pos)
            screen.blit(Timeline.chartsizesurface, (Timeline.pos[0]+128, Timeline.pos[1]+5))
            if Timeline.rect.collidepoint(mouse_pos):
                if mouse_pos[0] in range(Timeline.pos[0]+34, Timeline.pos[0]+82):
                    pass
                elif mouse_pos[0] in range(Timeline.pos[0]+82, Timeline.pos[0]+118):
                    pass
            screen.blit(Timeline.psurface, (Timeline.pos[0]+35, Timeline.pos[1]+8))
        def changeprices():
            Timeline.psurface.fill((255, 0, 0))
            Timeline.psurface.set_colorkey((255, 0, 0))
            draw.line(Timeline.psurface, (0, 0, 0), (44, 0), (44, 109))
            Timeline.psurface.blit(Description.font.render(str(round(Timeline.resrcval(S_ENERGY, 'buy'), 1)), False, (0, 0, 0)), (0, 4))
            Timeline.psurface.blit(Description.font.render(str(round(Timeline.resrcval(S_ENERGY, 'sell'), 1)), False, (0, 0, 0)), (48, 4))
            Timeline.psurface.blit(Description.font.render(str(round(Timeline.resrcval(S_FUEL, 'buy'), 1)), False, (0, 0, 0)), (0, 60))
            Timeline.psurface.blit(Description.font.render(str(round(Timeline.resrcval(S_FUEL, 'sell'), 1)), False, (0, 0, 0)), (48, 60))
            Timeline.psurface.blit(Description.font.render(str(round(Timeline.resrcval(S_COMP, 'buy'), 1)), False, (0, 0, 0)), (0, 32))
            Timeline.psurface.blit(Description.font.render(str(round(Timeline.resrcval(S_COMP, 'sell'), 1)), False, (0, 0, 0)), (48, 32))
            Timeline.psurface.blit(Description.font.render(str(round(Timeline.resrcval(S_PRODUCT, 'sell'), 1)), False, (0, 0, 0)), (48, 88))
            
        def gametick_update():
            temp=Timeline.chartsurface.copy()
            Timeline.chartsurface.fill((240, 240, 240))
            Timeline.chartsurface.blit(temp, (-24, 0))
            for i in [S_ENERGY, S_PRODUCT, S_COMP, S_FUEL]:
                draw.line(Timeline.chartsurface, Timeline.resrccolors[i], (2048-72, 118-Timeline.lastvalues[i]*56), (2048-48, 118-Timeline.resrcval(i)*56))
                Timeline.lastvalues[i]=Timeline.resrcval(i)
            Timeline.chartsizesurface.blit(Timeline.chartsurface, (Timeline.chartsizesurface.get_size()[0]-2048, 0))
            Timeline.changeprices()
    
    
    DOLLARS=0
    COAL=1
    URANIUM=2
    ANTI=3
    COMP1=4
    COMP2=5
    COMP3=6
    PRODUCT=7
    ENERGY=8
    ANTI_PROGRESS=9
    S_FUEL=0
    S_COMP=1
    S_PRODUCT=2
    S_ENERGY=3
    class Stats():
        game_resources=   [1000, 0, 0, 0  , 0  , 0  , 0  , 0  , 0, 0]
        resources_buybaseprice=  [1  , 2  , 3  , 130 , 2  , 5  , 10 , 14 , 1  ]
        resources_sellbaseprice= [1  , 2  , 3  , 130 , 2  , 5  , 11 , 14 , 1  ]
        resources_buyprice= [1  , 2  , 3  , 45 , 2  , 5  , 10 , 14 , 1  ]
        resources_sellprice= [1  , 2  , 3  , 45 , 2  , 5  , 11 , 14 , 1  ]
        resources_buymulti=[1]*4
        resources_sellmulti=[1.1]*4
        resource_pointers=   [0  , 8  , 7  , 4  , 5  , 6  , 1  , 2  , 3  ]
        resorces_data=dict(eval(open("./config/terrains.txt").read()))
        gametick_resorces=[[]]*9
        anti_discovered=False
        harvested_plants=0
        max_hrvest_plants=10
        resources_apocalypse_prices=False
        class GUI():
            isbuttonactive=False
            selectionstrip=None
            surface, usurface, ssurface, rect, buttrect=None, None, None, None, None
            pos=(0, 0)
            size=(0, 0)
            @staticmethod
            def build():
    #            f=open("./config/terrains.txt")
    #            resources_data=dict(eval(f.read()))
    #            f.close()
                Stats.selectionstrip=Surface((Stats.GUI.size[0], 32))
                Stats.selectionstrip.fill((255, 255, 255))
                Stats.GUI.rect=Rect(Stats.GUI.pos, Stats.GUI.size)
                GUIrects.append(Stats.GUI.rect)
                Stats.GUI.usurface=Surface(Stats.GUI.size)
                Stats.GUI.usurface.blit(menu_background, (0, 0))
                Stats.GUI.surface=Surface(Stats.GUI.size)
                Stats.GUI.surface.fill((255, 0, 0))
                Stats.GUI.surface.set_colorkey((255, 0, 0))
                Stats.display_update()
                Stats.GUI.surface.blit(image.load("./icons/dollar.gif").convert(), (4, 4))
                Stats.GUI.surface.blit(image.load("./icons/energy.gif").convert(), (4, 4+32))
                Stats.GUI.surface.blit(image.load("./icons/product.gif").convert(), (4, 4+32*2))
                Stats.GUI.surface.blit(image.load("./icons/component1.gif").convert(), (4, 4+32*3))
                Stats.GUI.surface.blit(image.load("./icons/component2.gif").convert(), (4, 4+32*4))
                Stats.GUI.surface.blit(image.load("./icons/component3.gif").convert(), (4, 4+32*5))
                Stats.GUI.surface.blit(image.load("./icons/coal.gif").convert(), (4, 4+32*6))
                Stats.GUI.surface.blit(image.load("./icons/uranium.gif").convert(), (4, 4+32*7))
                temp_s=Surface((64, 32*9))
                temp_s.fill((255,0,0))
                temp_s.set_colorkey((255,0,0))
                if Stats.anti_discovered:
                    Stats.GUI.surface.blit(image.load("./icons/antimatter.gif").convert(), (4, 4+32*8))
                    temp_s.blit(buy_icon, (26, 4+32*8))
                else:
                    Stats.GUI.surface.blit(image.load("./icons/unknown.gif").convert(), (4, 4+32*8))
                Stats.GUI.buttrect=Rect((Stats.GUI.size[0]-24*2-4, 0), (64, 32*9))
                temp_s.blit(sell_icon, (0, 4+32))
                temp_s.blit(buy_icon, (26, 4+32))
                temp_s.blit(sell_icon, (0, 4+32*2))
                temp_s.blit(sell_icon, (0, 4+32*3))
                temp_s.blit(buy_icon, (26, 4+32*3))
                temp_s.blit(sell_icon, (0, 4+32*4))
                temp_s.blit(buy_icon, (26, 4+32*4))
                temp_s.blit(sell_icon, (0, 4+32*5))
                temp_s.blit(buy_icon, (26, 4+32*5))
                temp_s.blit(sell_icon, (0, 4+32*6))
                temp_s.blit(buy_icon, (26, 4+32*6))
                temp_s.blit(buy_icon, (26, 4+32*7))
                Stats.GUI.surface.blit(temp_s, Stats.GUI.buttrect)
            @staticmethod
            def update():
                screen.blit(Stats.GUI.usurface, Stats.GUI.pos)
                if Stats.GUI.rect.collidepoint(mouse_pos):
                    if ((not ((mouse_pos[1]-Stats.GUI.pos[1])%32 in list(range(4))+list(range(24, 32)) or 
                      (mouse_pos[0]-Stats.GUI.buttrect.left) in range(24, 26))) and Stats.GUI.buttrect.collidepoint(mouse_pos)) and (
                      (not mouse_pos[1]>32*8) or (mouse_pos[1]>32*8 and Stats.anti_discovered)) and mouse_pos[1]>32 and (
                      not(mouse_pos[0] in range(230, 260) and (mouse_pos[1] in range(64, 96)))) and (
                      not(mouse_pos[0] in range(200, 230) and (mouse_pos[1] in range(228, 246+32)))):
                        Stats.GUI.isbuttonactive=True
                        if mouse_pos[0] in range(Stats.GUI.size[0]-52, Stats.GUI.size[0]-52+24):
                            screen.blit(selection24, (Stats.GUI.size[0]-52, int(mouse_pos[1]/32)*32+4))
                            Description.change(Stats.resource_pointers[int(mouse_pos[1]/32)], 0)
                        else:
                            screen.blit(selection24, (Stats.GUI.size[0]-26, int(mouse_pos[1]/32)*32+4))
                            Description.change(Stats.resource_pointers[int(mouse_pos[1]/32)], 1)
                    else:
                        Stats.GUI.isbuttonactive=False
                        screen.blit(Stats.selectionstrip, (Stats.GUI.pos[0], int(mouse_pos[1]/32)*32))
                        Description.change(Stats.resource_pointers[int(mouse_pos[1]/32)])
                else:
                    Stats.GUI.isbuttonactive=False
                screen.blit(Stats.GUI.ssurface, (Stats.GUI.pos[0], Stats.GUI.pos[1]))
                screen.blit(Stats.GUI.surface, Stats.GUI.pos)
            @staticmethod
            def buttonaction(xy):
                val='all' if SHIFT_pressed else 10
                if xy[1] in range(1, 8):
                    BuildMenu.toogledbutton=False
                    Tool.mode=Tool.SELECT
                    Description.resetforcelevel()
                    if xy[0]==0:
                        Stats.sell(Stats.resource_pointers[xy[1]], val)
                    else:
                        Stats.buy(Stats.resource_pointers[xy[1]], val)
                elif xy[1]==8 and Stats.anti_discovered:
                    BuildMenu.toogledbutton=False
                    Tool.mode=Tool.SELECT
                    Description.resetforcelevel()
                    if xy[0]==0:
                        Stats.sell(ANTI, val)
                    else:
                        Stats.buy(ANTI, val)
        @staticmethod
        def display_update():
            rsrc=Stats.game_resources
            f=get_correct_string_num
            temp_s=Surface((Stats.GUI.size[0]-32-64, Stats.GUI.size[1]))
            temp_s.fill((255, 0, 0))
            temp_s.set_colorkey((255, 0, 0))
            temp_s.blit(normaltext.render(f(rsrc[0]), False, (0, 0, 0)), (0, 6))
            temp_s.blit(normaltext.render(f(rsrc[8]), False, (0, 0, 0)), (0, 6+32))
            temp_s.blit(normaltext.render(f(rsrc[7]), False, (0, 0, 0)), (0, 6+32*2))
            temp_s.blit(normaltext.render(f(rsrc[4]), False, (0, 0, 0)), (0, 6+32*3))
            temp_s.blit(normaltext.render(f(rsrc[5]), False, (0, 0, 0)), (0, 6+32*4))
            temp_s.blit(normaltext.render(f(rsrc[6]), False, (0, 0, 0)), (0, 6+32*5))
            temp_s.blit(normaltext.render(f(rsrc[1]), False, (0, 0, 0)), (0, 6+32*6))
            temp_s.blit(normaltext.render(f(rsrc[2]), False, (0, 0, 0)), (0, 6+32*7))
            temp_s.blit(normaltext.render(f(rsrc[3]), False, (0, 0, 0)), (0, 6+32*8))
            Stats.GUI.ssurface=Surface((Stats.GUI.size[0]-32-64, Stats.GUI.size[1]))
            Stats.GUI.ssurface.fill((255, 0, 0))
            Stats.GUI.ssurface.set_colorkey((255, 0, 0))
            Stats.GUI.ssurface.blit(temp_s, (32, 0))
        def get_multi(index, buy_sell):
            if buy_sell=="buy":
                multi_source=Stats.resources_buymulti
            elif buy_sell=="sell":
                multi_source=Stats.resources_sellmulti
            if index in [COAL, URANIUM, ANTI]:
                return multi_source[S_FUEL]
            elif index in [COMP1, COMP2, COMP3]:
                return multi_source[S_COMP]
            elif index==PRODUCT:
                return multi_source[S_PRODUCT]
            elif index == ENERGY:
                return multi_source[S_ENERGY]
        @staticmethod
        def gametick_update():
            l1=Stats.game_resources
            for terrain in terrains:
                l2=terrains[terrain]["resources"]
                for _ in range(terrains[terrain]["count"]):
                    breaked=False
                    for i in range(min([len(l1), len(l2)])):
                        if l1[i]+l2[i]<0:
                            breaked=True
                            break
                    if breaked: break
                    for i in range(min([len(l1), len(l2)])):
                        l1[i]+=l2[i]
            Stats.game_resources=l1
            Stats.display_update()
            if Stats.game_resources[9]>=100:
                Stats.anti_discovered=True
                Stats.GUI.build()
            for i in range(1, 9):
                Stats.resources_buyprice[i]=Stats.resources_buybaseprice[i]*Stats.get_multi(i, 'buy')
                Stats.resources_sellprice[i]=Stats.resources_sellbaseprice[i]*Stats.get_multi(i, 'sell')
            for i in [S_FUEL, S_COMP, S_PRODUCT, S_ENERGY]:
                if not Stats.resources_apocalypse_prices:
                    if Stats.resources_buymulti[i]>1.1:
                        Stats.resources_buymulti[i]-=randint(1, 100)/300
                    elif Stats.resources_buymulti[i]<0.9:
                        Stats.resources_buymulti[i]+=randint(1, 100)/300
                    else:
                        Stats.resources_buymulti[i]+=randint(-10, 25)/300
                    if Stats.resources_sellmulti[i]>1.15:
                        Stats.resources_sellmulti[i]-=randint(1, 100)/300
                    if Stats.resources_sellmulti[i]<0.95:
                        Stats.resources_sellmulti[i]+=randint(1, 100)/300
                    else:
                        Stats.resources_sellmulti[i]-=randint(-10, 25)/300
            if randint(1, 1000)==234:
                Stats.max_hrvest_plants+=1
            Timeline.gametick_update()
        @staticmethod
        def buy(resourceindex, amount=0):
            "Buys resources, amount can be 'all'"
            r=resourceindex
            if Stats.game_resources[DOLLARS]>=Stats.resources_buyprice[resourceindex]:
                if amount!='all':
                    if Stats.game_resources[DOLLARS]-Stats.resources_buyprice[r]*amount>=0:
                        Stats.game_resources[DOLLARS]-=Stats.resources_buyprice[r]*amount
                        Stats.game_resources[r]+=amount
                        Stats.display_update()
                    else:
                        Stats.buy(resourceindex, 'all')
                else:
                    q=int(Stats.game_resources[DOLLARS]/Stats.resources_buyprice[r])
                    Stats.game_resources[DOLLARS]-=q*Stats.resources_buyprice[r]
                    Stats.game_resources[r]+=q
                    Stats.display_update()
            else:
                Description.change("Can't afford", "", True)
        @staticmethod
        def sell(resourceindex, amount='all'):
            "Sells resources, amount can be 'all'"
            r=resourceindex
            if amount=='all':
                Stats.game_resources[DOLLARS]+=Stats.resources_sellprice[r]*Stats.game_resources[r]
                Stats.game_resources[r]=0
            else:
                if amount>Stats.game_resources[r]:
                    Stats.sell(r, 'all')
                else:
                    Stats.game_resources[DOLLARS]+=Stats.resources_sellprice[r]*amount
                    Stats.game_resources[r]-=amount
            Stats.display_update()
    
    
    def split_for_size(string, size, warning=False):
        fs=Description.fontrect.size
        font=Description.font
        rowspace=int(size/fs[0])-1
        ostring=""
        for line in string.splitlines():
            for word in line.split():
                if rowspace>=len(word):
                    ostring+=word.replace("|", " ")+" "
                    rowspace-=(len(word)+1)
                else:
                    rowspace=int(size/fs[0])-(len(word)+2)
                    ostring+="\n"+word.replace("|", " ")+" "
            ostring+="\n"
        ln=ostring.splitlines()
        s=Surface((size, fs[1]*len(ln)))
        s.fill((255, 0, 0))
        s.set_colorkey((255, 0, 0))
        i=0
        if warning:
            color=ERRORRED
        else:
            color=(0, 0, 0)
        for line in ln:
            s.blit(font.render(line, False, color), (0, fs[1]*i))
            i+=1
        return s
    def delete_spaces_from_end(string):
        
        return string.rstrip()
    ERRORRED=(181, 33, 33)
    class Description():
        thistickhandled=False
        usurface, surface=None, None
        pos, size=(0, 0), (0, 0)
        d, db="", ""
        dname=["", "", 0]
        font=font.Font(font.match_font('Courier New'), 16)
        fontrect=font.render("A", False, (0, 0, 0)).get_rect()
        resources_stringdata=None
        @staticmethod
        def build():
            f=open("./config/resources.txt")
            Description.resources_stringdata=list(eval(f.read()))
            f.close()
            Description.usurface=Surface(Description.size)
            Description.usurface.blit(menu_background, (0, 0))
            draw.line(Description.usurface, (0, 0, 0), (4, 2), (Description.size[0]-4, 2))
            draw.line(Description.usurface, (0, 0, 0), (4, Description.size[1]-2), (Description.size[0]-4, Description.size[1]-2))
            GUIrects.append(Rect(Description.pos, Description.size))
            Description.change("Welcome to Factor&Co.", "Factor&Co.")
        @staticmethod
        def update():
            screen.blit(Description.usurface, Description.pos)
            screen.blit(Description.surface, Description.pos)
        @staticmethod
        def change(objectname, parameter=None, warning=False, forcelevel=0):
            if (Description.dname!=[objectname, parameter, forcelevel] or not Description.thistickhandled) and Description.dname[2]<=forcelevel:
                if objectname in list(terrains):
                    Description.d=terrains[objectname]["description"]
                    Description.db=delete_spaces_from_end(objectname)
                    Description.dname=[objectname, None, forcelevel]
                elif isinstance(objectname, int):
                    if parameter==None:
                        Description.d=Description.resources_stringdata[objectname][1]
                        Description.db=Description.resources_stringdata[objectname][0]
                        Description.dname=[objectname, None, forcelevel]
                    else:
                        Description.d=Description.resources_stringdata[objectname][2+parameter].format(round(Stats.resources_sellprice[objectname], 2), round(Stats.resources_buyprice[objectname], 2))
                        Description.db=Description.resources_stringdata[objectname][0]
                        Description.dname=[objectname, parameter, forcelevel]
                else:
                    Description.d=objectname
                    Description.db=str(parameter)
                    Description.dname=[objectname, parameter, forcelevel]
                Description.surface=Surface(Description.size)
                Description.surface.fill((255, 0, 0))
                Description.surface.set_colorkey((255, 0, 0))
                Description.font.set_bold(True)
                Description.font.set_underline(True)
                if warning:
                    Description.surface.blit(split_for_size(Description.db, Description.size[0], True), (4, 6))
                else:
                    Description.surface.blit(split_for_size(Description.db, Description.size[0], False), (4, 6))
                Description.font.set_bold(False)
                Description.font.set_underline(False)
                if warning:
                    Description.surface.blit(split_for_size(Description.d, Description.size[0], True), (4, 6+Description.fontrect.height))
                else:
                    Description.surface.blit(split_for_size(Description.d, Description.size[0]), (4, 6+Description.fontrect.height))
                Description.thistickhandled=True
        def resetforcelevel(newlevel=0):
            Description.dname[2]=newlevel
    class Tool():
        mode=3
        data=["Standard Factory", terrains["Standard Factory"]["build price"]]
        
        BUILD=1
        DESTROY=2
        SELECT=3
        @staticmethod
        def action(mode=None, data=None):
            if mode==None: mode=Tool.mode
            if data==None: data=Tool.data
            if not GamePause.paused:
                if not True in [r.collidepoint(event.pos) for r in GUIrects]:
                    if Tool.mode==Tool.BUILD:
                        if  m[0]>=View.world_pos[0] and m[0]<View.world_pos[0]+(var.current_world.size[0])*32 and (
                          m[1]>=View.world_pos[1] and m[1]<View.world_pos[0]+(var.current_world.size[1])*32 ):
                            if var.current_world.tilemap[int((m[0]-View.world_pos[0])/32)][int((m[1]-View.world_pos[1])/32)].build_price==None:
                                if data[1]==None:
                                    data[1]=0
                                dol=Stats.game_resources[DOLLARS]
                                if dol>=data[1]:
                                    active_tile=var.current_world.tilemap[int((m[0]-View.world_pos[0])/32)][
                                        int((m[1]-View.world_pos[1])/32)]
                                    active_tile.config(data[0])
                                    dol-=data[1]
                                    dol-=active_tile.demolition_price
                                    terrains[data[0]]["count"]+=1
                                    var.current_world.update()
                                    Stats.game_resources[DOLLARS]=dol
                                    Description.resetforcelevel()
                                    if active_tile.terrainname!="Concrete":
                                        Stats.harvested_plants+=1
                                    Description.change(Tool.data[0]+" has been sucessfully placed on map.", "Building placed")
                                    Stats.display_update()
                                    Tool.mode=Tool.SELECT
                                    Description.resetforcelevel()
                                else:
                                    Description.change("You don't have enough money to buy "+data[0]+" (you have $"+str(Stats.game_resources[DOLLARS])+", you need $"+str(data[1])+").", "Can't buy", True, 2)
                                
                            else:
                                Description.change("Can't place building on other building. Please select other tile for "+Tool.data[0]+".", "Invalid destination", True, 2)
                                Description.resetforcelevel(1)
                            
                    elif Tool.mode==Tool.DESTROY:
                        if  m[0]>=View.world_pos[0] and m[0]<View.world_pos[0]+(var.current_world.size[0])*32 and (
                          m[1]>=View.world_pos[1] and m[1]<View.world_pos[0]+(var.current_world.size[1])*32 ):
                            if var.current_world.tilemap[int((m[0]-View.world_pos[0])/32)][int((m[1]-View.world_pos[1])/32)].terrainname!="Concrete":
                                dol=Stats.game_resources[DOLLARS]
                                active_tile=var.current_world.tilemap[int((m[0]-View.world_pos[0])/32)][int((m[1]-View.world_pos[1])/32)]
                                if active_tile.demolition_price<=dol and dol>=Tool.data[1]:
                                    can_demolish=True
                                    old_terrname=active_tile.terrainname
                                    terrains[active_tile.terrainname]["count"]-=1
                                    demol_price="$"+str(active_tile.demolition_price) if active_tile.demolition_price!=0 else "free"
                                    _oldname=active_tile.terrainname
                                    _oldcost=active_tile.build_price
                                    dol-=active_tile.demolition_price
                                    
                                    
                                    Description.change("Tile with "+old_terrname+" has been demolished for "+demol_price+". 'BOOM' - the tile said.", "Tile demolished", False, 2)
                                    if _oldname!="Concrete" and _oldcost==None:
                                        if Stats.harvested_plants+1<=Stats.max_hrvest_plants:
                                            Stats.harvested_plants+=1
                                        else:
                                            can_demolish=False
                                            Description.change("You have killed too many trees. You can't kill more.", "Can't kill plants", True, 2)
                                    
                                    if can_demolish:
                                        active_tile.config("Concrete")
                                        var.current_world.update()
                                        Stats.game_resources[DOLLARS]=dol
                                        Stats.display_update()
                                else:
                                    Description.change("You don't have enough money to demolish "+var.current_world.tilemap[int((m[0]-View.world_pos[0])/32)][int((m[1]-View.world_pos[1])/32)].terrainname+".", "Out of money", True, 2)
                    elif Tool.mode==Tool.SELECT:
                         pass
                else:
                    if BuildMenu.rect.collidepoint(mouse_pos):
                        if BuildMenu.isbuttonactive:
                            BuildMenu.buttonaction(int((mouse_pos[1]-32)/32))
                    elif Stats.GUI.buttrect.collidepoint(mouse_pos):
                        if Stats.GUI.isbuttonactive:
                            Stats.GUI.buttonaction((int((mouse_pos[0]-Stats.GUI.buttrect.left)/26), int(mouse_pos[1]/32)))
            else:
                if GamePause.rect.collidepoint(mouse_pos):
                    if GamePause.isbuttonactive:
                        GamePause.buttonaction(int((mouse_pos[1]-GamePause.pos[1])/32))
                if CreditsView.active and CreditsView.brect.collidepoint(mouse_pos):
                    CreditsView.close()
                    
    class GamePause():
        backimagepos=[0, 0]
        backimagespeed=[-1, 1]
        role='pause'
        isbuttonactive=False
        selectionbutton=Surface((256, 24))
        rect=Rect((0, 0), (0, 0))
        size=(0, 0)
        pos=(0, 0)
        paused=False
        locksurface=Surface((1, 1))
        buttsurface=Surface((1, 1))
        bgsurface=Surface((1, 1))
        def build():
            GamePause.selectionbutton.fill((255, 255, 255))
            GamePause.rect=Rect((0, 0), GamePause.size)
            GamePause.locksurface=Surface(GamePause.size)
            GamePause.locksurface.fill((255, 255, 0))
            GamePause.locksurface.set_colorkey((255, 255, 0))
            GamePause.bgsurface=Surface((648, 1200))
            if GamePause.role=='pause':
                for x in range(6):
                    for y in range(6):
                        GamePause.locksurface.blit(menu_background, (x*512, y*512))
            else:
                try:
                    GamePause.world
                except AttributeError: #world doesn't exist
                    GamePause.world=World("GAMEMENU", [128, 128], random=True)
                
            for x in range(2):
                for y in range(6):
                    GamePause.bgsurface.blit(menu_background, (x*512, y*512))
            draw.rect(GamePause.locksurface, (0, 0, 0), Rect((int(GamePause.size[0]/2-128), int(GamePause.size[1]/2-96/2)), (256, 96)), 1)
            GamePause.locksurface.blit(factorcologo, (int(GamePause.size[0]/2)-320, GamePause.pos[1]/2))
            draw.rect(GamePause.locksurface, (0, 0, 0), Rect((int(GamePause.size[0]/2)-320, GamePause.pos[1]/2), (640, 128)), 1)
            
            GamePause.pos=(int(GamePause.size[0]/2)-128, int(GamePause.size[1]/2)-96/2)
            GamePause.buttsurface=Surface((256, 32*3))
            GamePause.buttsurface.fill((255, 0, 0))
            GamePause.buttsurface.set_colorkey((255, 0, 0))
            if GamePause.role=='pause':
                GamePause.locksurface.blit(Description.font.render("Game is paused.", False, (0, 0, 0)), (int(GamePause.size[0]/2)-75, GamePause.pos[1]-32))
                GamePause.buttsurface.blit(Description.font.render('Continue game', False, (0, 0, 0)), (4, 8))
                GamePause.buttsurface.blit(Description.font.render('Save and quit', False, (0, 0, 0)), (4, 32+8))
                GamePause.buttsurface.blit(Description.font.render('Quit without saving', False, (0, 0, 0)), (4, 64+8))
            elif GamePause.role=='mainmenu':
                GamePause.buttsurface.blit(Description.font.render('Play or continue', False, (0, 0, 0)), (4, 8))
                GamePause.buttsurface.blit(Description.font.render('Quit game', False, (0, 0, 0)), (4, 32+8))
                GamePause.buttsurface.blit(Description.font.render('Credits', False, (0, 0, 0)), (4, 64+8))
        def init():
            GamePause.paused=True
        def deinit():
            GamePause.paused=False
        def switch():
            if GamePause.paused:
                GamePause.deinit()
            else:
                GamePause.init()
        def update():
            if GamePause.role=='mainmenu':
                screen.blit(GamePause.world.groundsurface, GamePause.backimagepos)
                screen.blit(GamePause.world.basicsurface, (GamePause.backimagepos[0], GamePause.backimagepos[1]-64))
            screen.blit(GamePause.bgsurface, (int(GamePause.size[0]/2-324), 0))
            screen.blit(GamePause.locksurface, (0, 0))
            
            buttpos=GamePause.pos
            if mouse_pos[0] in range(int(GamePause.size[0]/2-128), int(GamePause.size[0]/2+128)) and (
               mouse_pos[1] in range(int(GamePause.pos[1]), int(GamePause.pos[1]+96))):
                screen.blit(Stats.selectionstrip, (buttpos[0], int((mouse_pos[1]-buttpos[1])/32)*32+buttpos[1]))
                GamePause.isbuttonactive=True
            else:
                GamePause.isbuttonactive=False
            screen.blit(GamePause.buttsurface, GamePause.pos)
        def buttonaction(y):
            if GamePause.role=='pause':
                if y==0:
                    GamePause.deinit()
                elif y==1:
                    var.current_world.save()
                    GamePause.changetomenu()
                    EmptyScreen.activate(1200)
                elif y==2:
                    GamePause.changetomenu()
            elif GamePause.role=='mainmenu':
                if y==0:
                    var.current_world=World("current_world", size=[16, 16])
                    var.current_world.update()
                    GamePause.role='pause'
                    GamePause.build()
                    GamePause.deinit()
                    EmptyScreen.activate()
                elif y==1:
                    pygame.quit()
                elif y==2:
                    CreditsView._open_()
                    EmptyScreen.activate(300)
        def changetomenu():
            var.current_world.resetchanges()
            GamePause.paused=True
            GamePause.role='mainmenu'
            GamePause.build()
            GamePause.MOVEEVENThandle()
        def MOVEEVENThandle():
            GamePause.backimagepos[0]=randint(-(4096-GamePause.size[0]), 0)
            GamePause.backimagepos[1]=randint(-(4096-GamePause.size[1]), 0)
    class CreditsView():
        surface=image.load("./othergraphics/credits.gif").convert()
        bgsurface=Surface((1, 1))
        bsurface=Surface((128, 32))
        btext=Surface((128, 32))
        brect=Rect((0, 0), (0, 0))
        whsurf=Surface((128, 32))
        whsurf.fill((255, 255, 255))
        active=False
        def build():
            CreditsView.bsurface.fill((255, 0, 0))
            CreditsView.bsurface.set_colorkey((255, 0, 0))
            draw.rect(CreditsView.bsurface, (0, 0, 0), Rect((0, 0), (128, 32)), 1)
            CreditsView.btext.fill((255, 0, 0))
            CreditsView.btext.set_colorkey((255, 0, 0))
            CreditsView.btext.blit(Description.font.render('Back', False, (0, 0, 0)), (4, 8))
            CreditsView.brect=Rect((int(GamePause.size[0]/2-384), int(GamePause.size[1]/2+336-32)), (128, 32))
            CreditsView.bgsurface=Surface(GamePause.size)
            for x in range(6):
                for y in range(6):
                    CreditsView.bgsurface.blit(menu_background, (x*512, y*512))
        def update():
            if CreditsView.active:
                screen.blit(CreditsView.bgsurface, (0, 0))
                screen.blit(CreditsView.surface, (int(GamePause.size[0]/2-384), int(GamePause.size[1]/2-336)))
                screen.blit(CreditsView.bsurface, CreditsView.brect)
                if CreditsView.brect.collidepoint(mouse_pos):
                    screen.blit(CreditsView.whsurf, CreditsView.brect)
                screen.blit(CreditsView.btext, CreditsView.brect)
        def close():
            CreditsView.active=False
        def _open_():
            CreditsView.active=True
    class PlaceManager():
        pos1, size1=(0, 0), (0, 0)
        pos2, size2=(0, 0), (0, 0)
        surface1, surface2=None, None
        def build():
            PlaceManager.surface1=Surface(PlaceManager.size1)
            PlaceManager.surface1.blit(menu_background, (0, 0))
            PlaceManager.surface1.blit(menu_background, (512, 0))
            PlaceManager.surface2=Surface(PlaceManager.size2)
            PlaceManager.surface2.blit(menu_background, (0, 0))
            PlaceManager.surface2.blit(menu_background, (512, 0))
            draw.line(PlaceManager.surface2, (0, 0, 0), (4, 2), (PlaceManager.size2[0]-4, 2))
            draw.line(PlaceManager.surface2, (0, 0, 0), (4, PlaceManager.size2[1]-2), (PlaceManager.size2[0]-4, PlaceManager.size2[1]-2))
            GUIrects.append(Rect(PlaceManager.pos1, PlaceManager.size1))
            GUIrects.append(Rect(PlaceManager.pos2, PlaceManager.size2))
        def update():
            screen.blit(PlaceManager.surface1, PlaceManager.pos1)
            screen.blit(PlaceManager.surface2, PlaceManager.pos2)
    class EmptyScreen():
        size=(1, 1)
        lsurf=Surface((1, 1))
        def build():
            EmptyScreen.lsurf=Surface(EmptyScreen.size)
            for x in range(6):
                for y in range(6):
                    EmptyScreen.lsurf.blit(menu_background, (x*512, y*512))
            EmptyScreen.lsurf.blit(Description.font.render("Loading...", False, (0, 0, 0)), (4, 8))
        def activate(delay=700):
            screen.blit(EmptyScreen.lsurf, (0, 0))
            display.update()
            import time
            time.sleep(delay/1000)
    factorcologo=image.load("./othergraphics/logo.gif").convert_alpha()
    def layout_update(size):
        screen=display.set_mode(size, RESIZABLE|DOUBLEBUF)
        if size[0]>=(512+256):
            if size[1]>=(512+32+128):
                BuildMenu.build()
                Stats.GUI.pos=(0, 0)
                Stats.GUI.size=(256, 32*9)
                Stats.GUI.build()
                Description.pos=(0, Stats.GUI.pos[1]+Stats.GUI.size[1])
                Description.size=(256, 256)
                Description.build()
                PlaceManager.pos1=(0, Description.pos[1]+Description.size[1])
                PlaceManager.size1=(256, size[1]-128-(Description.pos[1]+Description.size[1]))
                BuildMenu.pos=(size[0]-256, 0)
                BuildMenu.size=(256, 32*14)
                BuildMenu.build()
                PlaceManager.pos2=(size[0]-256, BuildMenu.pos[1]+BuildMenu.size[1])
                PlaceManager.size2=(256, size[1]-128-(BuildMenu.pos[1]+BuildMenu.size[1]))
                PlaceManager.build()
                Timeline.pos=(0, size[1]-128)
                Timeline.size=(size[0], 128)
                Timeline.build()
                w=View.world_pos
                s=var.current_world.size
                if w[0]>size[0]:
                    View.world_pos[0]=size[0]-8
                elif w[0]+s[0]<0:
                    View.world_pos[0]=8
                if w[1]>size[1]:
                    View.world_pos[1]=size[1]-8
                elif w[1]+s[1]<0:
                    View.world_pos[1]=8
                Stats.display_update()
                GamePause.size=size
                GamePause.build()
                GamePause.build() #I don't know why it must be called two times... 
                CreditsView.build()
                EmptyScreen.size=size
                EmptyScreen.build()
            else:
                screen=display.set_mode((size[0], 512+32+128), RESIZABLE|DOUBLEBUF)
                layout_update(screen.get_size())
        else:
            screen=display.set_mode((512+256, size[1]), RESIZABLE|DOUBLEBUF)
            layout_update(screen.get_size())
    
    class var():
        current_world=World("current_world", size=[16, 16])
        current_world.update()
    
    GUIrects=[]
    
    m=mouse_pos=(0,0)
    breaked=False
    
    GAMETICKEVENT=USEREVENT+1
    time.set_timer(GAMETICKEVENT, 1000)
    
    SHIFT_pressed=False
    
    layout_update(screen.get_size())
    View.build()
    
    GamePause.changetomenu()
    while True:
        for event in pygame.event.get():
            if   event.type==QUIT:
                if GamePause.role=='pause' and not GamePause.paused:
                    GamePause.init()
                elif GamePause.role=='pause':
                    GamePause.buttonaction(2) #Don't save and quit
                elif GamePause.role=='mainmenu':
                    pygame.quit()
                    breaked=True
                break
            elif event.type==VIDEORESIZE:
                layout_update(event.size)
            elif event.type==ACTIVEEVENT:
                pass
            elif event.type==MOUSEMOTION:
                m1=m
                m2=m=mouse_pos=event.pos
                if View.dragging and not GamePause.paused:
                    View.world_pos[0]+=(m2[0]-m1[0])
                    View.world_pos[1]+=(m2[1]-m1[1])
                del m1, m2
            elif event.type==KEYDOWN:
                if event.key==K_F1:
                    Stats.anti_discovered=not Stats.anti_discovered
                    Stats.GUI.build()
                    print(event.mod)
                if event.key==K_F2:
                    Stats.game_resources[9]+=100
                elif event.key==K_F2:
                    layout_update(screen.get_size())
                elif event.unicode!='':
                    if event.unicode in '!#$%^&*()':
                        Stats.GUI.buttonaction((1, ' !@#$%^&*()'.index(event.unicode)))
                    elif event.unicode in '134567890':
                        Stats.GUI.buttonaction((1, ' 1234567890'.index(event.unicode)))
                    elif event.unicode in 'QWERTYOP':
                        Stats.GUI.buttonaction((0, ' QWERTYUIOP'.index(event.unicode)))
                    elif event.unicode in 'qwertyop':
                        Stats.GUI.buttonaction((0, ' qwertyuiop'.index(event.unicode)))
                    elif event.unicode=='?':
                        Stats.game_resources[DOLLARS]+=500
                        Stats.display_update()
                    elif event.key==K_ESCAPE:
                        if Tool.mode!=Tool.SELECT:
                            Tool.mode=Tool.SELECT
                            BuildMenu.toogledbutton=False
                        else:
                            if GamePause.role=='pause':
                                GamePause.switch()
                elif event.key in (K_RSHIFT, K_LSHIFT):
                    SHIFT_pressed=True
            elif event.type==KEYUP:
                if event.key in (K_RSHIFT, K_LSHIFT):
                    SHIFT_pressed=False
            elif event.type==MOUSEBUTTONDOWN:
                if event.button==3:
                    if not True in [r.collidepoint(event.pos) for r in GUIrects]:
                        View.dragging=True
            elif event.type==MOUSEBUTTONUP:
                if event.button==1:
                    Tool.action()
                elif event.button==3:
                    View.dragging=False
            elif event.type==GAMETICKEVENT:
                if not GamePause.paused:
                    Stats.gametick_update()
                    Description.thistickhandled=False
            elif event.type==USEREVENT+2:
                print("Next")
                mixer.music.load("./soundtrack/"+choice(musics))
                #mixer.music.load("./soundtrack/test.mp3")
                mixer.music.play()
                #mixer.music.set_endevent(USEREVENT+2)
        if breaked:
            break
        if not GamePause.paused:
            screen.fill((0, 0, 0))
            View.update()
            BuildMenu.update()
            Stats.GUI.update()
            Description.update()
            PlaceManager.update()
            Timeline.update()
        else:
            GamePause.update()
            CreditsView.update()
        display.update()
        clock.tick(10)
#except pygame.error:
#    pass
#except Exception as exc:
#    print("The game has crashed by following exception:\n", exc)
finally:pass
